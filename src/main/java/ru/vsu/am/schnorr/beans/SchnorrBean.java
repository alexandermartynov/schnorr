package ru.vsu.am.schnorr.beans;

import java.math.BigInteger;
import java.util.Random;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class SchnorrBean {

	private BigInteger p = new BigInteger("9133337");
	private BigInteger q = new BigInteger("1141667");
	private BigInteger g = new BigInteger("1000008");
	
	private String alice = "";
	private String bob = "";
	
	public BigInteger getP() {
		return p;
	}
	
	public void setP(BigInteger p) {
		this.p = p;
	}
	
	public BigInteger getQ() {
		return q;
	}
	
	public void setQ(BigInteger q) {
		this.q = q;
	}
	
	public BigInteger getG() {
		return g;
	}
	
	public void setG(BigInteger g) {
		this.g = g;
	}
	
	public void schnorr() {
		BigInteger w = BigInteger.probablePrime(20,new Random());
		BigInteger y = g.modPow(w.negate(),p);
		StringBuilder stra = new StringBuilder();
		StringBuilder strb = new StringBuilder();
		stra.append("C�������� ���� �����: w = ")
			.append(w.toString())
			.append("\n")
			.append("������������� ���� �����: ")
			.append("\n")
			.append("p = ")
			.append(p.toString())
			.append("\n")
			.append("q = ")
			.append(q.toString())
			.append("\n")
			.append("g = ")
			.append(g.toString())
			.append("\n")
			.append("y = ")
			.append(y.toString())
			.append("\n")
			.append("\n");
		BigInteger r = BigInteger.probablePrime(20,new Random());
		BigInteger x = g.modPow(r,p);
		stra.append("����� ������� ����: � = ")
			.append(x.toString())
			.append("\n");
		BigInteger e = BigInteger.probablePrime(20,new Random());
		strb.append("��� ������ �����: e = ")
			.append(e.toString())
			.append("\n");
		BigInteger s = r.add(w.multiply(e)).mod(q);
		stra.append("����� ������� ����:  s = ")
			.append(s.toString())
			.append("\n");
		BigInteger z = g.modPow(s,p).multiply(y.modPow(e,p)).mod(p); 
		strb.append("��� �������� z: z = ")
			.append(z.toString())
			.append("\n");
		if(x.equals(z)){
			strb.append("����������� ������ �������");
		} else {
			strb.append("����������� �����������");
		}
		alice = stra.toString();
		bob = strb.toString();
	}

	public String getAlice() {
		return alice;
	}

	public void setAlice(String alice) {
		this.alice = alice;
	}

	public String getBob() {
		return bob;
	}

	public void setBob(String bob) {
		this.bob = bob;
	}
	
}
